update=17-Mar-18 3:54:44 PM
version=1
last_client=kicad
[pcbnew]
version=1
LastNetListRead=
PadDrlX=320
PadDimH=550
PadDimV=550
BoardThickness=620
TxtPcbV=600
TxtPcbH=600
TxtModV=500
TxtModH=500
TxtModW=100
VEgarde=100
DrawLar=120
EdgeLar=80
TxtLar=120
MSegLar=120
[pcbnew/libraries]
LibName1=sockets
LibName2=connect
LibName3=discret
LibName4=pin_array
LibName5=divers
LibName6=libcms
LibName7=display
LibName8=led
LibName9=dip_sockets
LibName10=pga_sockets
LibName11=valves
LibName12=automata
LibName13=opendous
LibName14=w_conn_d-sub
LibName15=w_conn_strip
LibName16=w_smd_strip
LibDir=../library
[general]
version=1
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[eeschema]
version=1
LibDir=../library;../library/kicad-symbols-master;../library/kicad-footprints-master
[eeschema/libraries]
LibName1=power
LibName2=device
LibName3=transistors
LibName4=conn
LibName5=linear
LibName6=regul
LibName7=74xx
LibName8=cmos4000
LibName9=adc-dac
LibName10=memory
LibName11=xilinx
LibName12=special
LibName13=microcontrollers
LibName14=dsp
LibName15=analog_switches
LibName16=motorola
LibName17=texas
LibName18=intel
LibName19=audio
LibName20=interface
LibName21=digital-audio
LibName22=philips
LibName23=display
LibName24=cypress
LibName25=siliconi
LibName26=opto
LibName27=atmel
LibName28=contrib
LibName29=valves
LibName30=automata
LibName31=opendous
LibName32=w_connectors
LibName33=kicad-symbols-master/4xxx
LibName34=kicad-symbols-master/4xxx_IEEE
LibName35=kicad-symbols-master/74xGxx
LibName36=kicad-symbols-master/74xx
LibName37=kicad-symbols-master/74xx_IEEE
LibName38=kicad-symbols-master/Amplifier_Audio
LibName39=kicad-symbols-master/Amplifier_Buffer
LibName40=kicad-symbols-master/Amplifier_Current
LibName41=kicad-symbols-master/Amplifier_Difference
LibName42=kicad-symbols-master/Amplifier_Instrumentation
LibName43=kicad-symbols-master/Amplifier_Operational
LibName44=kicad-symbols-master/Amplifier_Video
LibName45=kicad-symbols-master/Analog
LibName46=kicad-symbols-master/Analog_ADC
LibName47=kicad-symbols-master/Analog_DAC
LibName48=kicad-symbols-master/Analog_Switch
LibName49=kicad-symbols-master/Audio
LibName50=kicad-symbols-master/Battery_Management
LibName51=kicad-symbols-master/Comparator
LibName52=kicad-symbols-master/Connector_Generic
LibName53=kicad-symbols-master/Connector_Generic_Shielded
LibName54=kicad-symbols-master/Connector_Specialized
LibName55=kicad-symbols-master/Converter_ACDC
LibName56=kicad-symbols-master/Converter_DCDC
LibName57=kicad-symbols-master/CPLD_Altera
LibName58=kicad-symbols-master/CPLD_Xilinx
LibName59=kicad-symbols-master/CPU
LibName60=kicad-symbols-master/CPU_NXP_6800
LibName61=kicad-symbols-master/CPU_NXP_68000
LibName62=kicad-symbols-master/CPU_PowerPC
LibName63=kicad-symbols-master/Device
LibName64=kicad-symbols-master/Diode
LibName65=kicad-symbols-master/Diode_Bridge
LibName66=kicad-symbols-master/Diode_Laser
LibName67=kicad-symbols-master/Display_Character
LibName68=kicad-symbols-master/Display_Graphic
LibName69=kicad-symbols-master/Driver_Display
LibName70=kicad-symbols-master/Driver_FET
LibName71=kicad-symbols-master/Driver_LED
LibName72=kicad-symbols-master/Driver_Motor
LibName73=kicad-symbols-master/Driver_Relay
LibName74=kicad-symbols-master/DSP_Freescale
LibName75=kicad-symbols-master/DSP_Microchip_DSPIC33
LibName76=kicad-symbols-master/DSP_Motorola
LibName77=kicad-symbols-master/DSP_Texas
LibName78=kicad-symbols-master/Filter
LibName79=kicad-symbols-master/FPGA_Actel
LibName80=kicad-symbols-master/FPGA_Xilinx
LibName81=kicad-symbols-master/FPGA_Xilinx_Artix7
LibName82=kicad-symbols-master/FPGA_Xilinx_Kintex7
LibName83=kicad-symbols-master/FPGA_Xilinx_Spartan6
LibName84=kicad-symbols-master/FPGA_Xilinx_Virtex5
LibName85=kicad-symbols-master/FPGA_Xilinx_Virtex6
LibName86=kicad-symbols-master/FPGA_Xilinx_Virtex7
LibName87=kicad-symbols-master/GPU
LibName88=kicad-symbols-master/Graphic
LibName89=kicad-symbols-master/Interface
LibName90=kicad-symbols-master/Interface_CAN_LIN
LibName91=kicad-symbols-master/Interface_CurrentLoop
LibName92=kicad-symbols-master/Interface_Ethernet
LibName93=kicad-symbols-master/Interface_Expansion
LibName94=kicad-symbols-master/Interface_HID
LibName95=kicad-symbols-master/Interface_LineDriver
LibName96=kicad-symbols-master/Interface_Optical
LibName97=kicad-symbols-master/Interface_Telecom
LibName98=kicad-symbols-master/Interface_UART
LibName99=kicad-symbols-master/Interface_USB
LibName100=kicad-symbols-master/Isolator
LibName101=kicad-symbols-master/Isolator_Analog
LibName102=kicad-symbols-master/Jumper
LibName103=kicad-symbols-master/LED
LibName104=kicad-symbols-master/Logic_LevelTranslator
LibName105=kicad-symbols-master/Logic_Programmable
LibName106=kicad-symbols-master/MCU_AnalogDevices
LibName107=kicad-symbols-master/MCU_Atmel_8051
LibName108=kicad-symbols-master/MCU_Atmel_ATMEGA
LibName109=kicad-symbols-master/MCU_Atmel_ATTINY
LibName110=kicad-symbols-master/MCU_Atmel_AVR
LibName111=kicad-symbols-master/MCU_Cypress
LibName112=kicad-symbols-master/MCU_Espressif
LibName113=kicad-symbols-master/MCU_Infineon
LibName114=kicad-symbols-master/MCU_Intel
LibName115=kicad-symbols-master/MCU_Microchip_PIC10
LibName116=kicad-symbols-master/MCU_Microchip_PIC12
LibName117=kicad-symbols-master/MCU_Microchip_PIC16
LibName118=kicad-symbols-master/MCU_Microchip_PIC18
LibName119=kicad-symbols-master/MCU_Microchip_PIC24
LibName120=kicad-symbols-master/MCU_Microchip_PIC32
LibName121=kicad-symbols-master/MCU_Microchip_SAME
LibName122=kicad-symbols-master/MCU_Microchip_SAML
LibName123=kicad-symbols-master/MCU_Module
LibName124=kicad-symbols-master/MCU_Nordic
LibName125=kicad-symbols-master/MCU_NXP_ColdFire
LibName126=kicad-symbols-master/MCU_NXP_HC11
LibName127=kicad-symbols-master/MCU_NXP_HC12
LibName128=kicad-symbols-master/MCU_NXP_HCS12
LibName129=kicad-symbols-master/MCU_NXP_Kinetis
LibName130=kicad-symbols-master/MCU_NXP_LPC
LibName131=kicad-symbols-master/MCU_NXP_MAC7100
LibName132=kicad-symbols-master/MCU_NXP_MCore
LibName133=kicad-symbols-master/MCU_NXP_S08
LibName134=kicad-symbols-master/MCU_Parallax
LibName135=kicad-symbols-master/MCU_SiFive
LibName136=kicad-symbols-master/MCU_SiliconLabs
LibName137=kicad-symbols-master/MCU_ST_STM8
LibName138=kicad-symbols-master/MCU_ST_STM32
LibName139=kicad-symbols-master/MCU_Texas
LibName140=kicad-symbols-master/MCU_Texas_MSP430
LibName141=kicad-symbols-master/Mechanical
LibName142=kicad-symbols-master/Memory_Controller
LibName143=kicad-symbols-master/Memory_EEPROM
LibName144=kicad-symbols-master/Memory_EPROM
LibName145=kicad-symbols-master/Memory_Flash
LibName146=kicad-symbols-master/Memory_NVRAM
LibName147=kicad-symbols-master/Memory_RAM
LibName148=kicad-symbols-master/Memory_ROM
LibName149=kicad-symbols-master/Memory_UniqueID
LibName150=kicad-symbols-master/Motor
LibName151=kicad-symbols-master/Oscillator
LibName152=kicad-symbols-master/Potentiometer_Digital
LibName153=kicad-symbols-master/power
LibName154=kicad-symbols-master/Power_Management
LibName155=kicad-symbols-master/Power_Protection
LibName156=kicad-symbols-master/Power_Supervisor
LibName157=kicad-symbols-master/pspice
LibName158=kicad-symbols-master/Reference_Current
LibName159=kicad-symbols-master/Reference_Voltage
LibName160=kicad-symbols-master/Regulator_Controller
LibName161=kicad-symbols-master/Regulator_Current
LibName162=kicad-symbols-master/Regulator_Linear
LibName163=kicad-symbols-master/Regulator_SwitchedCapacitor
LibName164=kicad-symbols-master/Regulator_Switching
LibName165=kicad-symbols-master/Relay
LibName166=kicad-symbols-master/Relay_SolidState
LibName167=kicad-symbols-master/RF
LibName168=kicad-symbols-master/RF_AM_FM
LibName169=kicad-symbols-master/RF_Bluetooth
LibName170=kicad-symbols-master/RF_GPS
LibName171=kicad-symbols-master/RF_Mixer
LibName172=kicad-symbols-master/RF_Module
LibName173=kicad-symbols-master/RF_RFID
LibName174=kicad-symbols-master/RF_Switch
LibName175=kicad-symbols-master/RF_WiFi
LibName176=kicad-symbols-master/RF_ZigBee
LibName177=kicad-symbols-master/Sensor
LibName178=kicad-symbols-master/Sensor_Audio
LibName179=kicad-symbols-master/Sensor_Current
LibName180=kicad-symbols-master/Sensor_Gas
LibName181=kicad-symbols-master/Sensor_Humidity
LibName182=kicad-symbols-master/Sensor_Magnetic
LibName183=kicad-symbols-master/Sensor_Motion
LibName184=kicad-symbols-master/Sensor_Optical
LibName185=kicad-symbols-master/Sensor_Pressure
LibName186=kicad-symbols-master/Sensor_Proximity
LibName187=kicad-symbols-master/Sensor_Temperature
LibName188=kicad-symbols-master/Sensor_Touch
LibName189=kicad-symbols-master/Sensor_Voltage
LibName190=kicad-symbols-master/Switch
LibName191=kicad-symbols-master/Timer
LibName192=kicad-symbols-master/Timer_PLL
LibName193=kicad-symbols-master/Timer_RTC
LibName194=kicad-symbols-master/Transformer
LibName195=kicad-symbols-master/Transistor_Array
LibName196=kicad-symbols-master/Transistor_BJT
LibName197=kicad-symbols-master/Transistor_FET
LibName198=kicad-symbols-master/Transistor_IGBT
LibName199=kicad-symbols-master/Triac_Thyristor
LibName200=kicad-symbols-master/Valve
LibName201=kicad-symbols-master/Video
